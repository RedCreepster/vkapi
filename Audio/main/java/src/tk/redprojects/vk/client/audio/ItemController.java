package tk.redprojects.vk.client.audio;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.text.Text;
import tk.redprojects.vk.api.object.Audio;

import java.net.URL;
import java.util.ResourceBundle;

public class ItemController implements Initializable {
    public Hyperlink authorHyperlink;
    public Text nameTextField;
    public Button downloadButton;

    private Audio audio;
    private ArtistLinkClickListener artistLinkClickListener;
    private DownloadButtonClickListener downloadButtonClickListener;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setData(Audio audio, ArtistLinkClickListener artistLinkClickListener, DownloadButtonClickListener downloadButtonClickListener) {
        this.audio = audio;
        this.artistLinkClickListener = artistLinkClickListener;
        this.downloadButtonClickListener = downloadButtonClickListener;

        this.authorHyperlink.setText(audio.getArtist());
        this.nameTextField.setText(audio.getTitle());
    }

    public void artistClick() {
        artistLinkClickListener.onArtistLinkClicked(audio.getArtist());
    }

    public void downloadClick() {
        downloadButtonClickListener.onDownloadButtonClicked(audio.getArtist() + " - " + audio.getTitle(), audio.getUrl());
    }

    public void playClick() {
        Player.getInstance().play(audio.getUrl());
    }
}
