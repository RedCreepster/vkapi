package tk.redprojects.vk.client.audio;

public interface DownloadButtonClickListener {
    public void onDownloadButtonClicked(String name, String url);
}
