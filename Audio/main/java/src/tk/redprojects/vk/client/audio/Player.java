package tk.redprojects.vk.client.audio;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public class Player {
    private static Player instance = new Player();

    public static Player getInstance() {
        return instance;
    }

    private MediaPlayer mediaPlayer;

    public void play(String url) {
        this.stop();
        this.mediaPlayer = new MediaPlayer(new Media(url));
        this.mediaPlayer.play();
    }

    public void stop() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
        }
    }
}
