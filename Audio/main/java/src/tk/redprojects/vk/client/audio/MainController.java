package tk.redprojects.vk.client.audio;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;
import tk.redprojects.vk.api.VKAPI;
import tk.redprojects.vk.api.methods.Audio;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.*;

public class MainController implements Initializable {
    public TextField queryTextField;
    public Button searchButton;
    public VBox lisVBox;
    public ProgressBar progressBar;

    private Timer timer;
    private VKAPI vkapi;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        VKAPI.Auth auth = VKAPI.Auth.builder()
                .client_id(3383363)
                .display(VKAPI.Auth.Display.page)
                .redirect_uri(URI.create("https://oauth.vk.com/blank.html"))
                .scope(new ArrayList<>(Arrays.asList(new VKAPI.Auth.Scope[]{
                        VKAPI.Auth.Scope.audio,
                        VKAPI.Auth.Scope.offline
                })))
                .build();

        vkapi = VKAPI.builder()
                .access_token("c951c260b4a61173f9683d155b2aaf693945ab34674378b7084be1233c03d1a9618c132abfd6def66665d")
                .secret("c9e5a177e36e8e3c3f")
                .auth(auth)
                .build();

        new Thread(() -> {
            Audio.get search = Audio.get.builder().build();
            search.setVkapi(vkapi);
            Audio.get.Result result = search.result(Audio.get.Result.class);
            ArrayList<tk.redprojects.vk.api.object.Audio> items = result.getResponse().getItems();

            showItems(items);
        }).start();
    }

    public void preSearch() {
        if (timer != null) {
            timer.purge();
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timer.purge();
                timer.cancel();
                search();
            }
        }, 350);
    }

    public void search() {
        progressBar.setVisible(true);
        String queryString = queryTextField.getText();
        Audio.search search = Audio.search.builder().q(queryString).build();
        search.setVkapi(vkapi);
        Audio.search.Result result = search.result(Audio.search.Result.class);
        ArrayList<tk.redprojects.vk.api.object.Audio> items = result.getResponse().getItems();
        showItems(items);
    }

    private void showItems(ArrayList<tk.redprojects.vk.api.object.Audio> items) {
        Platform.runLater(() -> {
            lisVBox.getChildren().clear();
            items.forEach(audio -> {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    HBox item = fxmlLoader.load(getClass().getResource("/item.fxml").openStream());
                    fxmlLoader.<ItemController>getController().setData(audio, artist -> {
                        this.queryTextField.setText(artist);
                        search();
                    }, (name, url) -> {
                        lisVBox.setDisable(true);
                        progressBar.setVisible(true);
                        FileChooser fileChooser = new FileChooser();
                        fileChooser.setInitialFileName(name + ".mp3");
                        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("MP3 файлы", "mp3"));
                        File file = fileChooser.showSaveDialog(null);
                        new Thread(() -> {
                            if (file != null) {
                                try {
                                    FileUtils.copyURLToFile(new URL(url), file);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Dialog errorDialog = new Dialog();
                                    errorDialog.setContentText(e.getLocalizedMessage());
                                    errorDialog.show();
                                }
                            }
                            Platform.runLater(() -> {
                                lisVBox.setDisable(false);
                                progressBar.setVisible(false);
                            });
                        }).start();
                    });
                    lisVBox.getChildren().add(item);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            progressBar.setVisible(false);
        });
    }
}
