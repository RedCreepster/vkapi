package tk.redprojects.vk.client.audio;

public interface ArtistLinkClickListener {
    public void onArtistLinkClicked(String artist);
}
