package tk.redprojects.vk.api.methods;

import com.google.gson.JsonElement;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tk.redprojects.vk.api.Request;

@Data
@EqualsAndHashCode(callSuper = true)
public class Execute extends Request<Execute.Result> {

    private final String code;

    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class Result extends Request.Result {
        private final JsonElement response;
    }

}
