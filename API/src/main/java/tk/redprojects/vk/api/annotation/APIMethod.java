package tk.redprojects.vk.api.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface APIMethod {
    enum Method {
        GET, POST
    }

    boolean enableCache() default true;

    String path() default "";

    String jsonPattern() default "";

    Method method() default Method.GET;

    boolean requreAuth() default false;

    boolean loggingUrl() default true;

    boolean loggingResponseHeaders() default false;

    boolean loggingRequestHeaders() default false;
}
