package tk.redprojects.vk.api;

import com.sun.deploy.util.StringUtils;
import lombok.Builder;
import lombok.Data;
import tk.redprojects.vk.api.utils.Utils;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

@Builder
@Data
public class VKAPI {

    @Builder
    @Data
    public static class Auth {

        public enum Display {
            page, popup, mobile
        }

        public enum Scope {
            /**
             * Пользователь разрешил отправлять ему уведомления (для flash/iframe-приложений).
             */
            notify(1),
            /**
             * Доступ к друзьям.
             */
            friends(2),
            /**
             * Доступ к фотографиям.
             */
            photos(4),
            /**
             * Доступ к аудиозаписям.
             */
            audio(8),
            /**
             * Доступ к видеозаписям.
             */
            video(16),
            /**
             * Доступ к документам.
             */
            docs(131072),
            /**
             * Доступ к заметкам пользователя.
             */
            notes(2048),
            /**
             * Доступ к wiki-страницам.
             */
            pages(128),
            /**
             * Добавление ссылки на приложение в меню слева.
             */
            _256(256),
            /**
             * Доступ к статусу пользователя.
             */
            status(1024),
            /**
             * Доступ к предложениям (устаревшие методы).
             */
            offers(32),
            /**
             * Доступ к вопросам (устаревшие методы).
             */
            questions(64),
            /**
             * Доступ к обычным и расширенным методам работы со стеной.
             * Данное право доступа по умолчанию недоступно для сайтов при использовании OAuth-авторизации
             * (игнорируется при попытке авторизации).
             */
            wall(8192),
            /**
             * Доступ к группам пользователя.
             */
            groups(262144),
            /**
             * Доступ к расширенным методам работы с сообщениями (только для Standalone-приложений).
             */
            messages(4096),
            /**
             * Доступ к email пользователя.
             */
            email(4194304),
            /**
             * Доступ к оповещениям об ответах пользователю.
             */
            notifications(524288),
            /**
             * Доступ к статистике групп и приложений пользователя, администратором которых он является.
             */
            stats(1048576),
            /**
             * Доступ к расширенным методам работы с рекламным API.
             */
            ads(32768),
            /**
             * Доступ к товарам.
             */
            market(134217728),
            /**
             * Доступ к API в любое время (при использовании этой опции параметр expires_in, возвращаемый вместе с access_token, содержит 0 — токен бессрочный).
             */
            offline(65536),
            nohttps(0);

            private int bitMask;

            Scope(int bitMask) {
                this.bitMask = bitMask;
            }
        }

        public enum ResponsePairKey {
            access_token, expires_in, user_id, secret, https_required
        }

        private int client_id;
        private URI redirect_uri;
        private Display display = Display.page;
        private List<Scope> scope;
        private String state = "tk.redprojects.vk.api";
        private Boolean revoke = false;

        private final String response_type = "token";
        private final String v = "5.52";

        public String authUrl() {
            return "https://oauth.vk.com/authorize?client_id=" + client_id +
                    "&display=" + display.name() +
                    "&redirect_uri=" + redirect_uri +
                    "&scope=" + StringUtils.join(
                    Arrays.asList(Utils.enumArrayToStringArray(scope.toArray(
                            new Scope[scope.size()]
                    ))), ",") +
                    "&response_type=" + response_type +
                    "&v=" + v;
        }
    }

    private String access_token;
    private int expires_in;
    private int user_id;
    private String secret;
    private Boolean https_required;

    private Auth auth;

}
