package tk.redprojects.vk.api.object;

import lombok.Data;

@Data
public class Post {

    public static enum PostType {
        post, copy, reply, postpone, suggest
    }

    /**
     * Идентификатор записи.
     */
    private int id;
    /**
     * Идентификатор владельца стены, на которой размещена запись.
     */
    private int owner_id;
    /**
     * Идентификатор автора записи.
     */
    private int from_id;
    /**
     * Время публикации записи в формате unixtime.
     */
    private int date;
    /**
     * Текст записи.
     */
    private String text;
    /**
     * Идентификатор владельца записи, в ответ на которую была оставлена текущая.
     */
    private int reply_owner_id;
    /**
     * Идентификатор записи, в ответ на которую была оставлена текущая.
     */
    private int reply_post_id;
    /**
     * true, если запись была создана с опцией «Только для друзей».
     */
    private boolean friends_only;
    /**
     * Информация о комментариях к записи, объект с полями:
     */
//    private String comments;
//    private String likes;
//    private String reposts;
    private PostType post_type;
//    private String post_source;
//    private String attachments;
    private String geo;
    private int signer_id;
    private Post copy_history[];
    private boolean can_pin;
    private boolean can_delete;
    private boolean can_edit;
    private boolean is_pinned;

}
