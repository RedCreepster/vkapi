package tk.redprojects.vk.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import tk.redprojects.vk.api.utils.BooleanSerializer;
import tk.redprojects.vk.api.utils.ReflectionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Data
public abstract class Request<T extends Request.Result> {

    protected Gson gson = new Gson();

    protected VKAPI vkapi;

    @SuppressWarnings("WeakerAccess")
    public Request() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        BooleanSerializer serializer = new BooleanSerializer();
        gsonBuilder.registerTypeAdapter(Boolean.class, serializer);
        gsonBuilder.registerTypeAdapter(boolean.class, serializer);
        gson = gsonBuilder.create();
    }

    @SuppressWarnings("WeakerAccess")
    public String stringResult() {
        try {
            URL url = new URL(prepare());
            return IOUtils.toString(url, Charset.forName("utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public T result(Class<T> tClass) {
        return gson.fromJson(stringResult(), tClass);
    }

    @SuppressWarnings("WeakerAccess")
    public String prepare() {
        Class<? extends Request> clazz = this.getClass();
        String vkApiMethod = buildMethod(clazz);

        String params = buildParams();

        String path = "/method/" + vkApiMethod;

        String request = path + "?" + params;

        return prepareRequest(request);
    }

    @SuppressWarnings("WeakerAccess")
    protected String prepareRequest(String request) {
        if (vkapi != null && vkapi.getAuth().getScope().contains(VKAPI.Auth.Scope.nohttps)) {
            String sig = DigestUtils.md5Hex(request + vkapi.getSecret());

            return "http://api.vk.com" + request + "&sig=" + sig;
        } else {
            return "https://api.vk.com" + request;
        }
    }

    private String buildParams() {
        List<NameValuePair> params = new ArrayList<>();

        Field[] fields = this.getClass().getDeclaredFields();

        for (Field field : fields) {
            String fieldName = field.getName();
            if (fieldName.startsWith("_")) {
                fieldName = fieldName.substring(1);
            }
            try {
                Method method = ReflectionUtils.getGetter(field, getClass());

                Object value = method.invoke(this);

                if (value != null) {
                    value = ReflectionUtils.normalizeFieldValue(value, field);
                    BasicNameValuePair pair = new BasicNameValuePair(fieldName, String.valueOf(value));

                    params.add(pair);
                }
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (vkapi != null) {
            params.add(new BasicNameValuePair("access_token", vkapi.getAccess_token()));
            params.add(new BasicNameValuePair("v", vkapi.getAuth().getV()));
        }

        return URLEncodedUtils.format(params, Charset.defaultCharset());
    }

    private String buildMethod(Class<? extends Request> clazz) {
        StringBuilder vkApiMethod = new StringBuilder();

        if (clazz.getDeclaringClass() != null) {
            vkApiMethod
                    .append(clazz.getDeclaringClass().getSimpleName().toLowerCase())
                    .append(".");
        }

        String className = clazz.getSimpleName();
        vkApiMethod
                .append(String.valueOf(className.charAt(0)).toLowerCase())
                .append(className.substring(1));

        return vkApiMethod.toString();
    }

    public static abstract class Result {
    }

}
