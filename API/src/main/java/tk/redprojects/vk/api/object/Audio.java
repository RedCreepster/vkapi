package tk.redprojects.vk.api.object;

import lombok.Data;

@Data
public class Audio {
    private final int id;
    private final int owner_id;
    private final String artist;
    private final String title;
    private final long duration;
    private final String url;
    private final String lyrics_id;
    private final int album_id;
    private final int genre_id;
}
