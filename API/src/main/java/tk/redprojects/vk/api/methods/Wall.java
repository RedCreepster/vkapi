package tk.redprojects.vk.api.methods;

import lombok.*;
import tk.redprojects.vk.api.Request;
import tk.redprojects.vk.api.object.Post;

import java.util.ArrayList;

public class Wall {

    @Builder(toBuilder = true)
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class get extends Request<get.Result> {
        private int owner_id;
        private String domain;
        private int offset;
        private int count;
        private Filter filter;
        private boolean extended = false;
        private String fields[];

        public static enum Filter {
            /**
             * Предложенные записи на стене сообщества (доступно только при вызове с передачей access_token)
             */
            suggests,
            /**
             * Отложенные записи (доступно только при вызове с передачей access_token)
             */
            postponed,
            /**
             * Записи владельца стены
             */
            owner,
            /**
             * Записи не от владельца стены
             */
            others,
            /**
             * Все записи на стене (owner + others)
             */
            all
        }

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {

            @Data
            public static class Response {
                private Integer count;
                private ArrayList<Post> items;
            }

            @Setter(AccessLevel.PROTECTED)
            private Response response;
        }
    }


}
