package tk.redprojects.vk.api.ui;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import lombok.Getter;
import tk.redprojects.vk.api.VKAPI;

import javax.swing.*;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthDialog extends JFXPanel implements Runnable, EventHandler<WebEvent<String>> {
    private final VKAPI vkapi;
    private final Runnable cb;

    @Getter
    private JFrame frame;
    private WebView webView;
    private WebEngine webEngine;

    private AuthDialog(VKAPI vkapi, Runnable cb) {
        this.vkapi = vkapi;
        this.cb = cb;

        frame = new JFrame("Авторизация");
    }

    private void prepare() {
        webView = new WebView();
        webEngine = webView.getEngine();

        setScene(new Scene(webView));

        frame.add(this);
        frame.pack();

        frame.setSize(getSize());

        webEngine.setJavaScriptEnabled(true);
        webEngine.setOnStatusChanged(this);

        frame.setVisible(true);
    }

    private void end() {
        webEngine.setOnStatusChanged(null);
        webEngine = null;
        webView.setVisible(false);
        webView = null;
        frame.setVisible(false);
        frame = null;

        cb.run();
    }

    @Override
    public void run() {
        prepare();

        webEngine.load(vkapi.getAuth().authUrl());
    }

    @Override
    public void handle(WebEvent<String> event) {
        if (event.getEventType().getName().equals("WEB_STATUS_CHANGED")) {
            String location = webEngine.getLocation();

            if (location.startsWith(vkapi.getAuth().getRedirect_uri().toString())) {
                try {
                    URL url = new URL(location);
                    String ref = url.getRef();
                    System.out.println(ref);

                    String[] pairStrings = ref.split("&");

                    for (String pairString : pairStrings) {
                        String[] pairArray = pairString.split("=");
                        VKAPI.Auth.ResponsePairKey key = VKAPI.Auth.ResponsePairKey.valueOf(pairArray[0]);
                        String value = pairArray[1];

                        switch (key) {
                            case access_token:
                                vkapi.setAccess_token(value);
                                break;
                            case expires_in:
                                vkapi.setExpires_in(Integer.valueOf(value));
                                break;
                            case user_id:
                                vkapi.setUser_id(Integer.valueOf(value));
                                break;
                            case secret:
                                vkapi.setSecret(value);
                                break;
                            case https_required:
                                vkapi.setHttps_required(Boolean.valueOf(value));
                                break;
                        }
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } finally {
                    end();
                }
            }
        }
    }

    public static AuthDialog auth(VKAPI vkapi, Runnable cb) {
        AuthDialog authDialog = new AuthDialog(vkapi, cb);
        Platform.runLater(authDialog);

        return authDialog;
    }
}
