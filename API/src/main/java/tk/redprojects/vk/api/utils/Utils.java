package tk.redprojects.vk.api.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings("WeakerAccess")
public class Utils {

    public static <T extends Enum> String[] enumArrayToStringArray(T[] src) {
        List<String> result = new ArrayList<>();

        for (T element : src) {
            result.add(element.name());
        }

        return result.toArray(new String[result.size()]);
    }

    public static HashMap<String, String> splitQuery(String url) {
        HashMap<String, String> queryPairs = new HashMap<>();
        if (url.contains("?") || url.contains("#")) {
            char separator = url.contains("?") ? '?' : '#';
            String query = url.substring(url.indexOf(separator) + 1);
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                try {
                    queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return queryPairs;
    }

    public static String unixTime2StringDate(int unixSeconds) {
        return unixTime2StringDate(unixSeconds, false);
    }

    public static String unixTime2StringDate(int unixSeconds, boolean includeDate) {
        Date date = new Date(unixSeconds * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat(includeDate ? "yyyy-dd-MM HH:mm:ss" : "HH:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());

        return sdf.format(date);
    }

    public static String url2Base64(URL url) throws IOException {
        byte[] bytes = IOUtils.toByteArray(url);
        return org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
    }
}
