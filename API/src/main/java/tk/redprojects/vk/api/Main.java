package tk.redprojects.vk.api;

import tk.redprojects.vk.api.methods.Execute;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws URISyntaxException {
        VKAPI.Auth auth = VKAPI.Auth.builder()
                .client_id(3383363)
                .display(VKAPI.Auth.Display.page)
                .redirect_uri(new URI("https://oauth.vk.com/blank.html"))
                .scope(new ArrayList<>(Arrays.asList(new VKAPI.Auth.Scope[]{
                        VKAPI.Auth.Scope.messages,
                        VKAPI.Auth.Scope.nohttps,
                        VKAPI.Auth.Scope.offline
                })))
                .build();

        System.out.println(auth.authUrl());

        //https://oauth.vk.com/blank.html#access_token=97f6d18cc079310bbdda8678b74a0caddfd01d032c4cf50b5f652fe336383d008dfe86c9afc4c00a3dc33&expires_in=0&user_id=135982827
        VKAPI vkapi = VKAPI.builder()
                .access_token("8e92fc8db44717cfd12bab94ba98bd0113f1e3902f5c73c88a50c82ed81ffa4173e33c4221a3e59d55a41")
                .secret("c9e5a177e36e8e3c3f")
                .auth(auth)
                .build();

        Execute execute = new Execute("return {\"xz\":1};");
        execute.setVkapi(vkapi);
        Execute.Result je = execute.result(Execute.Result.class);

        System.out.println(je.getResponse());

//        AuthDialog authDialog = AuthDialog.auth(vkapi, new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("xz");
//            }
//        });

//        Wall.get wallGet = Wall.get.builder().count(10).owner_id(0).build();
//        wallGet.setVkapi(vkapi);
//
//        Wall.get.Result result = wallGet.result(Wall.get.Result.class);
//
//        for (Post post : result.getResponse().getItems()) {
//            if (!post.getText().isEmpty()) {
//                System.out.println(post.getText());
//            }
//        }

    }
}
