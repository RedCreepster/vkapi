package tk.redprojects.vk.api.methods;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import tk.redprojects.vk.api.Request;
import tk.redprojects.vk.api.object.Message;

import java.util.ArrayList;

public class Messages {

    @Data
    public static class MessageContainer {
        private Integer unread;
        private Message message;
        private Integer in_read;
        private Integer out_read;

        public boolean isChat() {
            return message.getChat_id() != 0;
        }
    }

    @Builder(toBuilder = true)
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class get extends Request<get.Result> {
        private boolean out;
        private Integer offset;
        private Integer count = 20;
        private Integer time_offset;
        private Integer filters = 0;
        private Integer preview_length;
        private Integer last_message_id;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {

            @Data
            public static class Response {
                private Integer count;
                private ArrayList<MessageContainer> items;
            }

            private Response response;
        }
    }

    @Builder(toBuilder = true)
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class getDialogs extends Request<getDialogs.Result> {
        private Integer offset;
        private Integer count = 20;
        private Integer start_message_id;
        private Integer preview_length;
        private boolean unread = false;
        private Integer user_id;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {

            @Data
            public static class Response {
                private Integer count;
                private Integer unread_dialogs;
                private ArrayList<MessageContainer> items;
            }

            @Getter
            private Response response;
        }
    }

    @Builder(toBuilder = true)
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class getById extends Request<getById.Result> {
        private Integer[] message_ids;
        private Integer preview_length = 0;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {

            @Data
            public static class Response {
                private Integer count;
                private ArrayList<MessageContainer> items;
            }

            @Getter
            private Response response;
        }
    }

    @Builder(toBuilder = true)
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class getHistory extends Request<getHistory.Result> {
        private Integer offset;
        private Integer count;
        private Integer user_id;
        private Integer peer_id;
        private Integer start_message_id;
        private boolean rev;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {

            @Data
            public static class Response {
                private Integer count;
                private Integer in_read;
                private Integer out_read;
                private ArrayList<Message> items;
            }

            @Getter
            private Response response;
        }
    }

    @Builder(toBuilder = true)
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class send extends Request<send.Result> {
        private String user_id;
        private String random_id;
        private String peer_id;
        private String domain;
        private String chat_id;
        private String user_ids;
        private String message;
        private String guid;
        private String lat;
        private String _long;
        private String attachment;
        private String forward_messages;
        private String sticker_id;
        private String notification;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {
            @Getter
            private int response;
        }
    }

}
