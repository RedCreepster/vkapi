package tk.redprojects.vk.api.methods;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tk.redprojects.vk.api.Request;

public class Utils {

    /**
     * Возвращает информацию о том, является ли внешняя ссылка заблокированной на сайте ВКонтакте.
     */
    @Data
    @EqualsAndHashCode(callSuper = true)
    @SuppressWarnings("WeakerAccess")
    public static class checkLink extends Request<checkLink.Result> {
        /**
         * Внешняя ссылка, которую необходимо проверить.
         */
        private final String url;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {
            @Data
            @SuppressWarnings("WeakerAccess")
            public static class Response {
                @SuppressWarnings("WeakerAccess")
                public static enum Status {
                    /**
                     * Ссылка не заблокирована
                     */
                    not_banned,
                    /**
                     * Ссылка заблокирована
                     */
                    banned,
                    /**
                     * Ссылка проверяется, необходимо выполнить повторный запрос через несколько секунд
                     */
                    processing;
                }

                private final Status status;
                private final String url;
            }

            private Response response;
        }
    }

    /**
     * Определяет тип объекта (пользователь, сообщество, приложение) и его идентификатор по короткому имени.
     */
    @Data
    @EqualsAndHashCode(callSuper = true)
    @SuppressWarnings("WeakerAccess")
    public static class resolveScreenName extends Request<resolveScreenName.Result> {
        /**
         * Короткое имя пользователя, группы или приложения.
         */
        private final String screen_name;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {
            @Data
            @SuppressWarnings("WeakerAccess")
            public static class Response {
                @SuppressWarnings("WeakerAccess")
                public static enum Type {
                    /**
                     * Пользователь
                     */
                    user,
                    /**
                     * Сообщество
                     */
                    group,
                    /**
                     * Приложение
                     */
                    application,
                    /**
                     * Страница
                     */
                    page;
                }

                /**
                 * Тип объекта.
                 */
                private final Type type;
                /**
                 * Идентификатор объекта.
                 * Если короткое имя screen_name не занято, то будет возвращён пустой объект.
                 */
                @SerializedName("object_id")
                private final int objectId;
            }

            private Response response;
        }
    }

    /**
     * Возвращает текущее время на сервере ВКонтакте в unixtime.
     */
    @Data
    @EqualsAndHashCode(callSuper = true)
    @SuppressWarnings("WeakerAccess")
    public static class getServerTime extends Request<getServerTime.Result> {
        /**
         * Внешняя ссылка, которую необходимо проверить.
         */
        private final String url;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {
            /**
             * Число, соответствующее времени в unixtime.
             */
            private int response;
        }
    }

}
