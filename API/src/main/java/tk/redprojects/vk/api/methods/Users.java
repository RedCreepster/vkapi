package tk.redprojects.vk.api.methods;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tk.redprojects.vk.api.Request;
import tk.redprojects.vk.api.object.User;

import java.util.ArrayList;

public class Users {

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class get extends Request<Users.get.Result> {

        public static enum NameCase {
            /**
             * Именительный
             */
            nom,
            /**
             * Родительный
             */
            gen,
            /**
             * Дательный
             */
            dat,
            /**
             * Винительный
             */
            acc,
            /**
             * Творительный
             */
            ins,
            /**
             * Предложный
             */
            abl,
        }

        public static enum Field {
            photo_id, verified, sex, bdate, city, country, home_town, has_photo,
            photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig,
            online, lists, domain, has_mobile, contacts, site, education, universities, schools,
            status, last_seen, followers_count, common_count, occupation, nickname, relatives,
            relation, personal, connections, exports, wall_comments, activities, interests, music,
            movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio,
            can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed,
            timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military,
            blacklisted, blacklisted_by_me
        }

        private final String[] user_ids;
        private final Field[] fields;
        private final NameCase name_case;

        @EqualsAndHashCode(callSuper = true)
        @Data
        public static class Result extends Request.Result {
            private final ArrayList<User> response;
        }
    }

}
