package tk.redprojects.vk.api.object;

import com.google.gson.JsonElement;
import lombok.Data;

import java.net.URL;

@Data
public final class User{
    private final int id;
    private final String first_name;
    private final String last_name;

    private final int sex;
    private final String nickname;
    private final String maiden_name;
    private final String domain;
    private final String screen_name;
    private final String bdate;
    private final JsonElement city;
    private final JsonElement country;
    private final URL photo_50;
    private final URL photo_100;
    private final URL photo_200;
    private final URL photo_max;
    private final URL photo_200_orig;
    private final URL photo_400_orig;
    private final URL photo_max_orig;
    private final String photo_id;
    private final boolean has_photo;
    private final boolean has_mobile;
    private final boolean is_friend;
    private final JsonElement friend_status;
    private final JsonElement online;
    private final JsonElement wall_comments;
    private final JsonElement can_post;
    private final JsonElement can_see_all_posts;
    private final JsonElement can_see_audio;
    private final JsonElement can_write_private_message;
    private final JsonElement can_send_friend_request;
    private final JsonElement mobile_phone;
    private final JsonElement home_phone;
    private final String site;
    private final JsonElement status;
    private final JsonElement last_seen;
    private final JsonElement crop_photo;
    private final JsonElement verified;
    private final JsonElement followers_count;
    private final JsonElement blacklisted;
    private final JsonElement blacklisted_by_me;
    private final JsonElement is_favorite;
    private final JsonElement is_hidden_from_feed;
    private final JsonElement common_count;
    private final JsonElement career;
    private final JsonElement military;
    private final JsonElement university;
    private final JsonElement university_name;
    private final JsonElement faculty;
    private final JsonElement faculty_name;
    private final JsonElement graduation;
    private final JsonElement home_town;
    private final JsonElement relation;
    private final JsonElement personal;
    private final String interests;
    private final String music;
    private final String activities;
    private final String movies;
    private final String tv;
    private final String books;
    private final String games;
    private final JsonElement universities;
    private final JsonElement schools;
    private final String about;
    private final JsonElement relatives;
    private final String quotes;
}
