package tk.redprojects.vk.api.object;

import lombok.Data;

import java.net.URL;
import java.util.ArrayList;

@Data
public class Message {

    @Data
    public static class PushSettings {
        private final int sound;
        private final int disabled_until;
    }

    private int id;
    private int user_id;
    private int from_id;
    private int date;
    private boolean read_state;
    private boolean out = false;
    private String title;
    private String body;
    private String geo;

    //TODO: доделать
    //    private String attachments;
    private ArrayList<Message> fwd_messages;
    private boolean emoji;
    private boolean important;
    private boolean deleted;
    private String random_id;

    // Extended
    private int chat_id;
    /**
     * Идентификаторы авторов последних сообщений беседы.
     */
    private String chat_active[];
    /**
     * Настройки уведомлений для беседы, если они есть.
     * sound и disabled_until
     */
    private PushSettings push_settings;
    private int users_count;
    private int admin_id;
    /**
     * Поле передано, если это служебное сообщение
     * Может быть chat_photo_update или chat_photo_remove, chat_create, chat_title_update, chat_invite_user, chat_kick_user
     */
    private String action;
    /**
     * Идентификатор пользователя (если > 0) или email (если < 0), которого пригласили или исключили
     * Для служебных сообщений с action равным chat_invite_user или chat_kick_user
     */
    private int action_mid;
    /**
     * Email, который пригласили или исключили
     * Для служебных сообщений с action равным chat_invite_user или chat_kick_user и отрицательным action_mid
     */
    private String action_email;
    /**
     * для служебных сообщений с action равным chat_create или chat_title_update
     */
    private String action_text;
    private URL photo_50;
    private URL photo_100;
    private URL photo_200;
}
