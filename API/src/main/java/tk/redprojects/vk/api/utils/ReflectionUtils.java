package tk.redprojects.vk.api.utils;

import tk.redprojects.vk.api.annotation.EnumConfig;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@SuppressWarnings("WeakerAccess")
public class ReflectionUtils {

    public static Method getGetter(Field field, Class<?> clazz) throws NoSuchMethodException {
        String fieldName = field.getName();

        String methodPrefix;

        Class<?> fieldType = field.getType();
        if (fieldType.equals(boolean.class) || fieldType.equals(Boolean.class)) {
            methodPrefix = "is";
        } else {
            methodPrefix = "get";
        }

        String methodName = methodPrefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());

        return clazz.getMethod(methodName);
    }

    public static Object getFieldValue(Field field, Object o) throws IllegalAccessException {
        field.setAccessible(true);
        return field.get(o);
    }

    public static String getterName(Field field) {
        String fieldName = field.getName();

        String methodPrefix;

        Class fieldType = field.getType();
        if (fieldType.equals(boolean.class) || fieldType.equals(Boolean.class) || fieldType.equals(AtomicBoolean.class)) {
            methodPrefix = "is";
        } else {
            methodPrefix = "get";
        }

        return methodPrefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
    }

    public static String normalizeFieldValue(Object value, Field field) {
        return normalizeFieldValue(value, field.getType());
    }

    public static String normalizeFieldValue(Object value, Class aClass) {
        if (aClass.equals(boolean.class) || aClass.equals(Boolean.class)) {
            if (value == null) {
                return "false";
            }
            return String.valueOf(Boolean.valueOf(value.toString()) ? 1 : 0);
        }
        if (aClass.equals(int.class) || aClass.equals(Integer.class)) {
            return String.valueOf(Integer.valueOf(value.toString()));
        }
        if (aClass.isEnum()) {
            if (aClass.isAnnotationPresent(EnumConfig.class)) {
                try {
                    EnumConfig config = ((EnumConfig) aClass.getAnnotation(EnumConfig.class));
                    String fieldOrMethod = config.fieldOrMethod();
                    try {
                        @SuppressWarnings("unchecked")
                        Method method = aClass.getMethod(fieldOrMethod);
                        return String.valueOf(method.invoke(value));
                    } catch (NoSuchMethodException ignored) {
                    }

                    try {
                        Field field = aClass.getField(fieldOrMethod);
                        return String.valueOf(field.get(value));
                    } catch (NoSuchFieldException ignored) {
                    }
                } catch (IllegalAccessException | InvocationTargetException ignored) {
                }
            }

            return ((Enum) value).name();
        }

        if (value.getClass().isArray()) {
            ArrayList<Object> values = new ArrayList<>();

            for (Object o : (Object[]) value) {
                values.add(normalizeFieldValue(o, o.getClass()));
            }

            value = join(values, ",");
        }

        return String.valueOf(value);
    }

    public static String join(List strings, CharSequence delimiter) {
        return join(strings.toArray(new Object[strings.size()]), delimiter);
    }

    public static String join(Object[] strings, CharSequence delimiter) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object string : strings) {
            stringBuilder
                    .append(string)
                    .append(delimiter);
        }

        stringBuilder.deleteCharAt(stringBuilder.length());

        return stringBuilder.toString();
    }
}
