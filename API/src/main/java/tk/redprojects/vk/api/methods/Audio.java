package tk.redprojects.vk.api.methods;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tk.redprojects.vk.api.Request;

import java.util.ArrayList;

public class Audio {

    @Data
    @EqualsAndHashCode(callSuper = true)
    @Builder(toBuilder = true)
    public static class search extends Request<search.Result> {
        private final String q;
        private final Integer offset;
        private final Integer count;

        @Data
        @EqualsAndHashCode(callSuper = true)
        public static class Result extends Request.Result {
            private Response response;

            @Data
            public static class Response {
                private final Integer count;
                private final ArrayList<tk.redprojects.vk.api.object.Audio> items;
            }
        }
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @Builder(toBuilder = true)
    public static class get extends Request<get.Result> {
        private final Integer owner_id;
        private final Integer offset;
        private final Integer count;

        @Data
        @EqualsAndHashCode(callSuper = true)
        public static class Result extends Request.Result {
            private Response response;

            @Data
            public static class Response {
                private final Integer count;
                private final ArrayList<tk.redprojects.vk.api.object.Audio> items;
            }
        }
    }
}
