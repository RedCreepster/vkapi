package tk.redprojects.vk.client.controllers;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;
import tk.redprojects.vk.api.utils.Utils;
import tk.redprojects.vk.client.Cache;

public class Message {
    public ImageView image;
    public Text text;
    public Text date;

    @Setter
    @Getter
    private tk.redprojects.vk.api.object.Message message;

    public void init() {
        image.setImage(new Image(Cache.getInstance().getUser(message.getFrom_id()).getPhoto_50().toExternalForm()));
        text.setText(message.getBody());
        date.setText(Utils.unixTime2StringDate(message.getDate()));
    }

}
