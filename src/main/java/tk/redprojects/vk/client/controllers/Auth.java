package tk.redprojects.vk.client.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import tk.redprojects.vk.api.VKAPI;
import tk.redprojects.vk.api.utils.Utils;
import tk.redprojects.vk.client.Config;

import java.util.HashMap;

public class Auth {

    @FXML
    public WebView webView;

    private final VKAPI vkapi = Config.getInstance().getAccount();

    @FXML
    public void initialize() {
        webView.setPrefSize(800, 600);
    }

    public void init(Runnable callBack) {
        WebEngine engine = webView.getEngine();

        engine.load(vkapi.getAuth().authUrl());

        engine.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
                if (newValue.equals(Worker.State.SUCCEEDED)) {
                    String location = engine.getLocation();
                    HashMap<String, String> params = Utils.splitQuery(location);
                    System.out.println(params);
                    if (params.containsKey("access_token")) {
                        vkapi.setAccess_token(params.get("access_token"));
                        vkapi.setExpires_in(Integer.valueOf(params.get("expires_in")));
                        vkapi.setUser_id(Integer.valueOf(params.get("user_id")));

                        observable.removeListener(this);

                        callBack.run();
                    }
                }
            }
        });
    }

}
