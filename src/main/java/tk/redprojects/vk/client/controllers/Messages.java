package tk.redprojects.vk.client.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;
import tk.redprojects.vk.client.Cache;

import java.io.IOException;

public class Messages {

    public VBox messages;
    @Getter
    @Setter
    public Pane mainPane;

    @Setter
    @Getter
    private boolean isChat = false;

    @Setter
    @Getter
    private int id;

    public void init() {
        Cache.getHistory(id, isChat, 15).getItems().forEach(this::addMessage);
    }

    private void addMessage(tk.redprojects.vk.api.object.Message message) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            Pane p = fxmlLoader.load(getClass().getResource("/message.fxml").openStream());
//            p.setMaxWidth(mainPane.getWidth());
            messages.getChildren().add(p);
            Message messageController = fxmlLoader.getController();

            messageController.setMessage(message);
            messageController.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
