package tk.redprojects.vk.client.controllers;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;
import tk.redprojects.vk.api.object.User;
import tk.redprojects.vk.client.Cache;
import tk.redprojects.vk.client.Config;
import tk.redprojects.vk.client.utils.Utils;

import java.io.IOException;

public class Dialog {

    public ImageView userImage;
    public Text username;
    public VBox messages;
    @Getter
    @Setter
    public Pane mainPane;
    public TextArea messageText;
    public ScrollPane scrollPane;
    public HBox userInfo;
    public HBox messageBox;
    public VBox xzpane;

    @Setter
    @Getter
    private boolean isChat = false;

    @Setter
    @Getter
    private int id;

    public void init() {
        userInfo.addEventHandler(EventType.ROOT, new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                System.out.println(xzpane.getPrefHeight());
            }
        });

        scrollPane.setPrefSize(
                mainPane.getPrefWidth(),
                mainPane.getPrefHeight()
                        - userInfo.getHeight() - userInfo.getPadding().getTop() - userInfo.getPadding().getBottom()
                        - (messageBox.getHeight()
                        - messageBox.getPadding().getTop()
                        - messageBox.getPadding().getBottom())
        );

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            messages.getChildren().add(fxmlLoader.load(getClass().getResource("/messages.fxml").openStream()));
            Messages messagesController = fxmlLoader.getController();

            messagesController.setChat(isChat);
            messagesController.setId(id);
            messagesController.setMainPane(mainPane);

            messagesController.init();

            User user = Cache.getUser(id);

            Utils.lazyLoadImage(userImage, user.getPhoto_50().toExternalForm(), "");
            username.setText(user.getFirst_name() + " " + user.getLast_name());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void messageSendButtonEvent(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            messageSend();
        }
    }

    public void messageSendKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            messageSend();
        }
    }

    private void messageSend() {
        if (!messageText.getText().trim().isEmpty()) {
            tk.redprojects.vk.api.methods.Messages.send send = tk.redprojects.vk.api.methods.Messages.send.builder()
                    .message(messageText.getText().trim())
                    .user_id(String.valueOf(id))
                    .build();
            send.setVkapi(Config.getInstance().getAccount());
            tk.redprojects.vk.api.methods.Messages.send.Result result = send.result(tk.redprojects.vk.api.methods.Messages.send.Result.class);
            System.out.println(result.getResponse());
        }
    }
}
