package tk.redprojects.vk.client.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import tk.redprojects.vk.api.VKAPI;
import tk.redprojects.vk.client.Config;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

public class Main {
    public Pane mainPane;

    private final VKAPI vkapi = Config.getInstance().getAccount();

    public void initialize() {
        mainPane.setPrefSize(800, 600);
        try {
            if (vkapi.getAccess_token() == null || vkapi.getAccess_token().isEmpty()) {
                auth();
                return;
            }
            dialogs();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void auth() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        WebView authPane = fxmlLoader.load(getClass().getResource("/auth.fxml").openStream());
        Auth authController = fxmlLoader.getController();

        VKAPI.Auth auth = VKAPI.Auth.builder()
                .client_id(3383363)
                .display(VKAPI.Auth.Display.page)
                .redirect_uri(URI.create(""))
                .scope(Arrays.asList(VKAPI.Auth.Scope.messages, VKAPI.Auth.Scope.offline))
                .build();

        vkapi.setAuth(auth);

        authController.init(() -> {
            mainPane.getChildren().remove(authPane);
            try {
                Config.getInstance().save();
            } catch (IOException e) {
                e.printStackTrace();
            }
            initialize();
        });

        mainPane.getChildren().add(authPane);
    }

    public void dialogs() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        mainPane.getChildren().add(fxmlLoader.load(getClass().getResource("/dialogs.fxml").openStream()));
        Dialogs dialogsController = fxmlLoader.getController();

        dialogsController.setMainPane(mainPane);
        dialogsController.init();
    }
}
