package tk.redprojects.vk.client.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;
import tk.redprojects.vk.api.VKAPI;
import tk.redprojects.vk.api.methods.Messages;
import tk.redprojects.vk.client.Config;

import java.io.IOException;

public class Dialogs {

    private final VKAPI vkapi = Config.getInstance().getAccount();

    public static Messages.getDialogs.Result result;

    @Getter
    @Setter
    public Pane mainPane;

    public VBox dialogs;
    public ScrollPane scrollPane;

    public void initialize() {
    }

    public void init() {
        scrollPane.setPrefSize(mainPane.getPrefWidth(), mainPane.getPrefHeight());
        Messages.getDialogs getDialogs = Messages.getDialogs.builder().count(15).build();
        getDialogs.setVkapi(vkapi);
        result = getDialogs.result(Messages.getDialogs.Result.class);

        result.getResponse().getItems().forEach(this::addDialog);
    }

    private void addDialog(Messages.MessageContainer messageContainer) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            dialogs.getChildren().add(fxmlLoader.load(getClass().getResource("/dialog_item.fxml").openStream()));
            DialogItem dialogItemController = fxmlLoader.getController();

            dialogItemController.setMessageContainer(messageContainer);
            dialogItemController.setMainPane(mainPane);
            dialogItemController.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
