package tk.redprojects.vk.client.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import tk.redprojects.vk.api.methods.Messages;
import tk.redprojects.vk.api.object.User;
import tk.redprojects.vk.client.Cache;
import tk.redprojects.vk.client.utils.Utils;

import java.io.IOException;

public class DialogItem {
    @Getter
    @Setter
    private Messages.MessageContainer messageContainer;

    @Getter
    @Setter
    public Pane mainPane;

    public ImageView userImage;
    public HBox dialogItemContainer;
    public Text userName;
    public ImageView fromImage;
    public Text lastMessage;

    public void init() {
        String messageBody = messageContainer.getMessage().getBody();
        if (!messageBody.isEmpty()) {
            lastMessage.setText(messageBody.replace("\n", " "));

            int userId = messageContainer.getMessage().getUser_id();

            val cache = Cache.getInstance();

            User user = cache.getUser(userId);

            Utils.lazyLoadImage(userImage, user.getPhoto_50().toExternalForm(),"");
            userName.setText(user.getFirst_name() + " " + user.getLast_name());
        } else {
            lastMessage.setText("Вложение");
        }
    }

    public void dialogSelect(MouseEvent mouseEvent) {
        System.out.println("dialogSelect" + messageContainer.getMessage().getUser_id());
        switch (mouseEvent.getButton()) {
            case PRIMARY:
                System.out.println(messageContainer.isChat());
                try {
                    openDialog();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void openDialog() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        mainPane.getChildren().clear();
        mainPane.getChildren().add(fxmlLoader.load(getClass().getResource("/dialog.fxml").openStream()));
        Dialog dialogController = fxmlLoader.getController();

        boolean isChat = messageContainer.isChat();
        dialogController.setChat(isChat);
        tk.redprojects.vk.api.object.Message message = messageContainer.getMessage();
        dialogController.setId(isChat ? message.getChat_id() : message.getUser_id());
        dialogController.setMainPane(mainPane);

        dialogController.init();
    }
}
