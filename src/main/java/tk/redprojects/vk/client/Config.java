package tk.redprojects.vk.client;

import com.google.gson.Gson;
import lombok.Data;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import tk.redprojects.vk.api.VKAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

@Data
public class Config {

    private static final File configFile = new File("config.json").getAbsoluteFile();
    private static final Gson gson = new Gson();
    @Getter
    private static final Config instance = load();

    private static Config load() {
        File parentFile = configFile.getParentFile();
        if (!parentFile.exists() || !parentFile.isDirectory()) {
            if (parentFile.mkdirs()) {
                new FileNotFoundException(parentFile.getAbsolutePath()).printStackTrace();
            }
        }
        if (configFile.exists()) {
            try {
                return gson.fromJson(FileUtils.readFileToString(configFile, Charset.forName("UTF-8")), Config.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Config(VKAPI.builder().build());
    }

    private final VKAPI account;

    public void save() throws IOException {
//        cache.entrySet().removeIf(entry -> entry.getValue().expires < new Date().getTime());
        FileUtils.writeStringToFile(configFile, gson.toJson(this), Charset.forName("UTF-8"));
    }
}
