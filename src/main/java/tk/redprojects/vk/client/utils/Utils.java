package tk.redprojects.vk.client.utils;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.HashMap;

public class Utils {

    private final static HashMap<String, Image> images = new HashMap<>();

    public static void lazyLoadImage(final ImageView imageView, final String url, final String lazyUrl) {
        if (images.containsKey(url)) {
            imageView.setImage(images.get(url));
            return;
        }
        if (lazyUrl != null && !lazyUrl.isEmpty()) {
            imageView.setImage(new Image(lazyUrl));
        }

        Image image = new Image(url, true);
        image.progressProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.doubleValue() == 1) {
                images.put(url, image);
                imageView.setImage(image);
            }
        });
    }

}
