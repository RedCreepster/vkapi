package tk.redprojects.vk.client;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import tk.redprojects.vk.api.methods.Messages;
import tk.redprojects.vk.api.methods.Users;
import tk.redprojects.vk.api.object.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;

@EqualsAndHashCode(callSuper = true)
@Data
public class Cache extends HashMap<String, Cache.CacheObject> {
    @Data
    public static class CacheObject {
        private final long expires;
        private final JsonElement value;

        public static CacheObject create(JsonElement value) {
            return new CacheObject(new Date().getTime() + 360 * 1000, value);
        }
    }

    private static final File cacheFile = new File("cache.json").getAbsoluteFile();
    private static final Gson gson = new Gson();

    @Getter
    private static Cache instance = load();

    private static Cache load() {

        File parentFile = cacheFile.getParentFile();
        if (!parentFile.exists() || !parentFile.isDirectory()) {
            if (parentFile.mkdirs()) {
                new FileNotFoundException(parentFile.getAbsolutePath()).printStackTrace();
            }
        }
        if (cacheFile.exists()) {
            try {
                return gson.fromJson(FileUtils.readFileToString(cacheFile, Charset.forName("UTF-8")), Cache.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return new Cache();
    }

    public void save() throws IOException {
        FileUtils.writeStringToFile(cacheFile, gson.toJson(this), Charset.forName("UTF-8"));
    }

    private Cache() {
    }

    public static User getUser(int userId) {
        if (!available(userId)) {
            Users.get get = new Users.get(
                    new String[]{String.valueOf(userId)},
                    Users.get.Field.values(),
                    Users.get.NameCase.nom
            );

            get.setVkapi(Config.getInstance().getAccount());

            Users.get.Result result = get.result(Users.get.Result.class);

            User user = result.getResponse().get(0);

            instance.put(String.valueOf(userId), CacheObject.create(gson.toJsonTree(user)));

            try {
                instance.save();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return user;
        }

        return gson.fromJson(instance.get(String.valueOf(userId)).getValue(), User.class);
    }

    public static Messages.getHistory.Result.Response getHistory(int id, boolean isChat, int count) {
        String key = id + String.valueOf(isChat) + count;
        if (!available(key)) {

            tk.redprojects.vk.api.methods.Messages.getHistory getHistory = tk.redprojects.vk.api.methods.Messages.getHistory.builder()
                    .user_id(id)
                    .count(count)
                    .build();
            getHistory.setVkapi(Config.getInstance().getAccount());

            Messages.getHistory.Result result = getHistory.result(Messages.getHistory.Result.class);

            instance.put(key, CacheObject.create(gson.toJsonTree(result.getResponse())));

            try {
                instance.save();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result.getResponse();
        }

        return gson.fromJson(instance.get(key).getValue(), Messages.getHistory.Result.Response.class);
    }

    public static boolean available(Object key) {
        return instance.containsKey(String.valueOf(key));
    }

    public static boolean availableAll(Object... keys) {
        for (Object key : keys) {
            if (!available(String.valueOf(key))) {
                return false;
            }
        }

        return true;
    }
}
